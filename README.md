# Weather Forecast

This small app will give you a weather forecast for the next five days, just have to enter your city name.

![Screen Shot](screen_shot.png)

## Tech stack

- [x] Code base: [React](https://reactjs.org/)
- [x] Types: [Typescript](https://www.typescriptlang.org/)
- [x] State management: [Redux](https://redux.js.org/) with [Thunk middleware](https://github.com/reduxjs/redux-thunk)
- [x] API communication: [axios](https://github.com/axios/axios)
- [x] Test: [Jest](https://jestjs.io/), [Testing Library](https://testing-library.com/) & [Enzyme](https://enzymejs.github.io/enzyme/#enzyme)

### Start Project

```
yarn start
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Run Test

```
yarn test
```

Launches the test runner in the interactive watch mode.

### Build

```
yarn build
```

Builds the app for production to the `build` folder.
