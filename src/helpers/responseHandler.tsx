import _get from "lodash/get";
import { DEFAULT_MESSAGE } from "../constants";

export const handleResponse = (response: any, config: any) => {
  const dispatch = _get(config, "dispatch", null);
  const type = _get(config, "type", "AXIOS");
  dispatch({
    type,
  });

  if (response) {
    dispatch({
      type: `${type}_SUCCESS`,
      payload: response,
    });
  } else {
    dispatch({
      type: `${type}_FAILED`,
      payload: DEFAULT_MESSAGE,
    });
  }

  return response;
};

export const handleError = (error: any, config: any) => {
  const dispatch = _get(config, "dispatch", null);
  const type = _get(config, "type", "AXIOS");

  dispatch({
    type: `${type}_FAILED`,
    payload: error.message,
  });

  return error;
};
