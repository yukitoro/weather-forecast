import _isEmpty from "lodash/isEmpty";

export const buildGetUrl = (root: string, url: string, params: any) => {
  let fullUrl = null;
  if (!root || !url) return null;
  fullUrl = root + url;
  if (!_isEmpty(params)) {
    fullUrl +=
      "?" +
      Object.keys(params)
        .map(function (prop) {
          return [prop, params[prop]].map(encodeURIComponent).join("=");
        })
        .join("&");
  }
  return fullUrl;
};
