import { handleError, handleResponse } from "./responseHandler";

import _get from "lodash/get";
import axios from "axios";

export const fetcher = async (url: string, config: any) => {
  return await axios({
    method: _get(config, "method", "GET"),
    url,
    data: _get(config, "data", {}),
    withCredentials: true,
    headers: {},
  })
    .then((response) => {
      const data = _get(response, "data", {});
      return handleResponse(data, config);
    })
    .catch((error) => handleError(error, config));
};
