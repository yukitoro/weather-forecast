import styles from "./styles.module.scss";

export default function PageTitle(): JSX.Element {
  return <h1 className={styles["page-title"]}>Weather Forecast</h1>;
}
