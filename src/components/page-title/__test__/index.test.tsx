import React from "react";
import PageTitle from "..";
import { render, screen } from "@testing-library/react";

describe("Search", () => {
  it("renders with correct text", () => {
    render(<PageTitle />);
    const text = screen.getByText(/Weather Forecast/i);
    expect(text).toHaveTextContent("Weather Forecast");
  });
});
