import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import styles from "./styles.module.scss";
import { useSelector } from "react-redux";
import _get from "lodash/get";
import _isEmpty from "lodash/isEmpty";
import { getDay } from "../../utils";

export function ForecastPlaceholder(): JSX.Element {
  return (
    <Col data-testid={"forecast-placeholder"}>
      <div className={styles["placeholder"]}>No Data</div>
    </Col>
  );
}

interface Day {
  id: number;
  applicable_date: string;
  min_temp: number;
  max_temp: number;
}

interface ListProps {
  data: Day[];
}

export function ForecastList(props: ListProps): JSX.Element {
  const { data } = props;

  return (
    <>
      {!_isEmpty(data) &&
        data.slice(0, 5).map((day: Day) => (
          <Col key={day.id} className={styles["col"]} data-testid={"day-info"}>
            <div className={styles["day-info"]}>
              <span>
                <strong>{getDay(day.applicable_date)}</strong>
              </span>
              <span>Min: {Math.round(day.min_temp)}°</span>
              <span>Max: {Math.round(day.max_temp)}°</span>
            </div>
          </Col>
        ))}
    </>
  );
}

interface City {
  name: string;
}

export function CityName(props: City): JSX.Element {
  const { name } = props;

  return (
    <>
      {!!name && (
        <h3 data-testid="city-name" className={styles["city-name"]}>
          {name}
        </h3>
      )}
    </>
  );
}

function Forecast(): JSX.Element {
  const info = useSelector((state) => _get(state, "weatherReducer.info", {}));

  const { consolidated_weather: forecastList, title: cityName } = info;

  return (
    <Container>
      <Row className={styles["row"]}>
        <CityName name={cityName} />
        {!_isEmpty(info) ? (
          <ForecastList data={forecastList} />
        ) : (
          <ForecastPlaceholder />
        )}
      </Row>
    </Container>
  );
}

export default Forecast;
