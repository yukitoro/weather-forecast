import React from "react";
import { cleanup, render } from "@testing-library/react";
import Forecast from "..";
import ForecastList from "..";
import ForecastPlaceholder from "..";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

afterEach(cleanup);

const mockStore = configureStore();

function renderWithRedux(component: any, state = {}) {
  return {
    ...render(<Provider store={mockStore(state)}>{component}</Provider>),
  };
}

describe("City Name", () => {
  it("is displayed when having weather info", () => {
    const store = mockStore({
      weatherReducer: {
        info: {
          title: "London",
        },
      },
    });
    const wrapper = mount(
      <Provider store={store}>
        <Forecast />
      </Provider>
    );
    expect(wrapper.find('[data-testid="city-name"]').text()).toEqual("London");
  });
});

describe("Forecast List", () => {
  it("renders five days forecast", () => {
    const mockState = {
      weatherReducer: {
        info: {
          consolidated_weather: [
            {
              id: 6650537127706624,
              applicable_date: "2021-09-08",
              min_temp: 17.439999999999998,
              max_temp: 28.335,
            },
            {
              id: 6080952691326976,
              applicable_date: "2021-09-09",
              min_temp: 17.119999999999997,
              max_temp: 23.34,
            },
            {
              id: 6579846663634944,
              applicable_date: "2021-09-10",
              min_temp: 16.145,
              max_temp: 21.155,
            },
            {
              id: 5271881214066688,
              applicable_date: "2021-09-11",
              min_temp: 15.754999999999999,
              max_temp: 21.78,
            },
            {
              id: 5921614228094976,
              applicable_date: "2021-09-12",
              min_temp: 13.365,
              max_temp: 19.41,
            },
          ],
          title: "London",
        },
      },
    };

    const { queryAllByTestId } = renderWithRedux(<ForecastList />, mockState);
    expect(queryAllByTestId("day-info").length).toEqual(5);
  });
});

describe("Forecast Placeholder", () => {
  it("renders when no data available", () => {
    const mockState = {
      weatherReducer: {
        info: {},
      },
    };
    const { queryByTestId } = renderWithRedux(
      <ForecastPlaceholder />,
      mockState
    );
    const placeholder = queryByTestId("forecast-placeholder");
    expect(placeholder).toHaveTextContent("No Data");
  });
});
