import { BrowserRouter as Router } from "react-router-dom";
import { renderRoutes } from "react-router-config";
import routes from "../../routes";
import styles from "./styles.module.scss";

function App(): JSX.Element {
  return (
    <div className={styles["weather-app"]}>
      <Router>{renderRoutes(routes)}</Router>
    </div>
  );
}

export default App;
