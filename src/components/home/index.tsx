import React from "react";
import Header from "../header";
import Forecast from "../forecast";
import Container from "react-bootstrap/Container";
import styles from "./styles.module.scss";

export default function Home(): JSX.Element {
  return (
    <Container className={styles["home"]}>
      <Header />
      <Forecast />
    </Container>
  );
}
