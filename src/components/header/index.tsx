import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import PageTitle from "../page-title";
import Search from "../search";

export default function Header(): JSX.Element {
  return (
    <Container>
      <Row>
        <Col xs={12} sm={8}>
          <PageTitle />
        </Col>
        <Col xs={12} sm={4}>
          <Search />
        </Col>
      </Row>
    </Container>
  );
}
