import React from "react";
import { cleanup } from "@testing-library/react";
import SearchSuggestion from "..";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

afterEach(cleanup);

const mockStore = configureStore();

describe("Search Suggestion", () => {
  it("is displayed when having cities data", () => {
    const store = mockStore({
      cityReducer: {
        cities: [
          {
            title: "London",
            woeid: 44418,
          },
        ],
      },
    });
    const wrapper = mount(
      <Provider store={store}>
        <SearchSuggestion />
      </Provider>
    );
    expect(wrapper.find('[data-testid="search-suggestion"]').length).toEqual(1);
  });
});
