import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import _get from "lodash/get";
import _isEmpty from "lodash/isEmpty";
import styles from "./styles.module.scss";
import { getWeatherInfo } from "./actions";

interface City {
  title: string;
  woeid: number;
}

function SearchSuggestion(): JSX.Element {
  const [displayed, setDisplayed] = useState(false);
  const dispatch = useDispatch();
  const cities = useSelector((state) => _get(state, "cityReducer.cities", ""));

  useEffect(() => {
    if (!_isEmpty(cities)) {
      setDisplayed(true);
    } else {
      setDisplayed(false);
    }
  }, [cities]);

  const handleChooseCity = (e: any, id: number) => {
    setDisplayed(false);
    dispatch(getWeatherInfo(id));
  };

  return (
    <>
      {displayed && (
        <div data-testid="search-suggestion" className={styles["suggestion"]}>
          {cities.map((city: City) => (
            <span
              key={city.woeid}
              onClick={(e) => handleChooseCity(e, city.woeid)}
            >
              {city.title}
            </span>
          ))}
        </div>
      )}
    </>
  );
}
export default SearchSuggestion;
