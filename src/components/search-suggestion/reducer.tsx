import { types } from "./types";

import Immutable from "seamless-immutable";
import _get from "lodash/get";

const initialState = Immutable({
  info: {},
});

const weatherReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case types.GET_WEATHER_INFO:
      return state.set("info", {});

    case types.GET_WEATHER_INFO_SUCCESS: {
      const info = _get(action, "payload", {});
      return state.set("info", info);
    }

    case types.GET_WEATHER_INFO_FAILED:
      return state.set("error", _get(action, "payload", ""));

    default:
      return state;
  }
};
export default weatherReducer;
