import { fetcher } from "../../helpers/fetcher";
import { DOMAIN, API_PATH } from "../../constants";

export const getWeatherInfo = (config: any) => {
  const { data } = config;
  let url = DOMAIN + API_PATH + "/location/" + data + "/";
  return fetcher(url, config);
};
