import { Dispatch } from "redux";
import { types } from "./types";
import * as apis from "./apis";

export const getWeatherInfo = (params: any) => (dispatch: Dispatch) => {
  const config = {
    type: types.GET_WEATHER_INFO,
    method: "GET",
    data: params,
    dispatch,
  };
  return apis.getWeatherInfo(config);
};
