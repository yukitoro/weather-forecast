import { types } from "./types";

import Immutable from "seamless-immutable";
import _get from "lodash/get";

const initialState = Immutable({
  cities: [],
  error: "",
});

const cityReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case types.GET_CITIES:
      return state.set("cities", []);

    case types.GET_CITIES_SUCCESS: {
      const cities = _get(action, "payload", []);
      return state.set("cities", cities);
    }

    case types.GET_CITIES_FAILED:
      return state.set("cities", []).set("error", _get(action, "payload", ""));

    case types.CLEAR_DATA:
      return state.set("cities", []);

    default:
      return state;
  }
};
export default cityReducer;
