import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import Search from "..";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import userEvent from "@testing-library/user-event";

configure({ adapter: new Adapter() });

afterEach(cleanup);

const mockStore = configureStore();

function renderWithRedux(component: any, state = {}) {
  return {
    ...render(<Provider store={mockStore(state)}>{component}</Provider>),
  };
}

describe("Search", () => {
  it("renders correctly", () => {
    const { queryByTestId, queryByPlaceholderText } = renderWithRedux(
      <Search />
    );
    expect(queryByPlaceholderText(`How's the weather in...`)).toBeTruthy();
    expect(queryByTestId("search-suggestion")).toBeNull();
  });
});

describe("Search input", () => {
  it("changes on input", () => {
    renderWithRedux(<Search />);
    const searchInput = screen.getByTestId("search-bar");
    userEvent.type(searchInput, "London");
    expect(searchInput).toHaveValue("London");
  });
});
