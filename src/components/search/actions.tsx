import { Dispatch } from "redux";
import { types } from "./types";
import * as apis from "./apis";

export const getCities = (params: any) => (dispatch: Dispatch) => {
  const config = {
    type: types.GET_CITIES,
    method: "GET",
    data: params,
    dispatch,
  };
  return apis.getCities(config);
};

export const clearData = () => (dispatch: Dispatch) =>
  dispatch({
    type: types.CLEAR_DATA,
    payload: [],
  });
