import { useRef, useEffect } from "react";
import Form from "react-bootstrap/Form";
import _get from "lodash/get";
import _debounce from "lodash/debounce";
import { useSelector, useDispatch } from "react-redux";
import { getCities, clearData } from "./actions";
import SearchSuggestion from "../search-suggestion";
import styles from "./styles.module.scss";

export default function Search(): JSX.Element {
  const dispatch = useDispatch();

  const cityName = useSelector((state) =>
    _get(state, "weatherReducer.info.title", "")
  );

  const searchRef = useRef<HTMLInputElement>(cityName);

  const handleSearch = () => {
    if (searchRef.current) {
      const { value } = searchRef.current;
      if (!!value) {
        dispatch(getCities({ query: value.trim() }));
      } else {
        dispatch(clearData());
      }
    }
  };

  const onSearching = _debounce(handleSearch, 500);

  useEffect(() => {
    if (cityName) {
      searchRef.current.value = cityName;
    }
  }, [cityName]);

  return (
    <Form className={styles["search"]}>
      <Form.Control
        data-testid="search-bar"
        ref={searchRef}
        type={"text"}
        placeholder={"How's the weather in..."}
        onChange={onSearching}
      />
      <SearchSuggestion data-testid={"search-suggestion"} />
    </Form>
  );
}
