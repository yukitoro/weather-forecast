import { buildGetUrl } from "../../helpers/api";
import { fetcher } from "../../helpers/fetcher";
import { DOMAIN, API_PATH } from "../../constants";

export const getCities = (config: any) => {
  const { data } = config;
  let url = DOMAIN + buildGetUrl(API_PATH, "/location/search/", data);
  return fetcher(url, config);
};
