import Home from "./components/home";

const routes = [
  {
    component: Home,
    routes: [
      { exact: true, path: "/", component: Home, name: "Weather Forecast" },
    ],
  },
];

export default routes;
