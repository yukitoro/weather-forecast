export const DOMAIN = process.env.REACT_APP_DOMAIN || "";
export const API_PATH = "/api";
export const DEFAULT_MESSAGE = "An error has occurred";
