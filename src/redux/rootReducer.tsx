import { combineReducers } from "redux";
import cityReducer from "../components/search/reducer";
import weatherReducer from "../components/search-suggestion/reducer";

export default combineReducers({
  cityReducer,
  weatherReducer,
});
